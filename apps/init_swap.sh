sudo touch /swapfile
sudo chmod 600 /swapfile
sudo truncate -s 0 /swapfile
sudo chattr +C /swapfile
sudo btrfs property set /swapfile compression none
sudo fallocate -l 2G /swapfile
sudo mkswap /swapfile
sudo swapon /swapfile
sudo swapon --show
